package com.lw.vrv.spi;

import java.util.ServiceLoader;

public class Main {
    public static void main(String[] args) {
        ServiceLoader<Search> loaders = ServiceLoader.load(Search.class);

        for (Search s : loaders) {
            System.out.println(s.getClass() + ", " + s.isSupport("db"));
        }
    }
}
